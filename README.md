### Install pgo
```
 helm install pgo1 .\helm\install\ -n pgo --create-namespace
```


```
db:
  spec:
    users:
    - name: postgres
      password:
        type: AlphaNumeric
      databases:
      - darknet
    # pgo tag is ubi8-pg_version-postgis_version-build_version
    # ref: https://access.crunchydata.com/documentation/postgres-operator/v5/references/components/#container-tags
    image: registry.developers.crunchydata.com/crunchydata/crunchy-postgres-gis:ubi8-15.4-3.3-0
    postgresVersion: 15
    databaseInitSQL:
      key: init.sql
      name: '{{ include "darknet.fullname" $ }}-init-sql'
    patroni:
      dynamicConfiguration:
        postgresql:
          pg_hba:
          - "hostnossl all all all md5"
    instances:
    - replicas: 1
      dataVolumeClaimSpec:
        accessModes:
        - "ReadWriteOnce"
        resources:
          requests:
            storage: "10Gi"
      resources:
        limits:
          memory: 4Gi
    backups:
      pgbackrest:
        repos:
        - name: repo1
          volume:
            volumeClaimSpec:
              accessModes:
              - "ReadWriteOnce"
              storageClassName:
              resources:
                requests:
                  storage: "50Gi"

```

```
apiVersion: postgres-operator.crunchydata.com/v1beta1
kind: PostgresCluster
metadata:
  name: "{{ include "darknet.fullname" . }}-db"
  annotations:
  labels:
    {{- include "darknet.labels" . | nindent 4 }}
spec:
  {{- tpl (toYaml .Values.db.spec) . | nindent 2 }}
```
##### Create postgres.yaml
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "darknet.fullname" $ }}-init-sql
  labels:
    {{- include "darknet.labels" $ | nindent 4 }}
    app.kubernetes.io/component: db
apiVersion: v1
data:
  {{- (.Files.Glob "files/init.sql").AsConfig | nindent 2 }}

```
##### Go into files/init.sql
```
\c darknet;

CREATE EXTENSION IF NOT EXISTS timescaledb;
```